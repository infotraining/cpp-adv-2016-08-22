#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <future>

using namespace std;

void may_throw()
{
    random_device rd;
    mt19937 random_engine(rd());
    uniform_int_distribution<> dist(1, 100);

    auto random_number = dist(random_engine);

    if (random_number % 13 == 0)
        throw runtime_error("Error#13");

    if (random_number % 4 == 0)
        throw logic_error("Error#4");
}

int calc_square(int x)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_int_distribution<> dist(500, 5000);

    may_throw();

    cout << "Starting calculation for: " << x << endl;
    this_thread::sleep_for(chrono::milliseconds(dist(gen)));

    return x * x;
}

string load_from_file(const string& filename)
{
    cout << "Opening " << filename << "..." << endl;

    random_device rd;
    mt19937_64 gen(rd());
    uniform_int_distribution<> dist(500, 5000);

    may_throw();

    this_thread::sleep_for(chrono::milliseconds(dist(gen)));

    return filename + " content";
}

void process_text(size_t id, shared_future<string> future_content)
{
    string content = future_content.get();

    cout << "THD#" << id << " is processing text: " << content << endl;
}

int main()
{
    future<int> f1 = async(launch::async, &calc_square, 42);

    while(f1.wait_for(50ms) != future_status::ready)
    {
        cout << "Main is working... waiting...." << endl;
    }

    int result = 0;
    try
    {
        result = f1.get();
    }
    catch (const exception& e)
    {
        cout << "Future has ended with " << e.what() << endl;
    }

    cout << "result = " << result << endl;

    vector<tuple<int, future<int>>> futures;

    for(int i = 1; i <= 10; ++i)
        futures.push_back(make_tuple(i, async(launch::async, &calc_square, i)));


    this_thread::sleep_for(1000ms);

    for(auto& f : futures)
    {
        try
        {
            cout << get<0>(f) << " * " << get<0>(f) << " = " << get<1>(f).get() << endl;

        }
        catch (const exception& e)
        {
            cout << "calc_square(" << get<0>(f) << ") has ended with " << e.what() << endl;
        }
    }

    cout << "\n\n";

    future<string> future_download = async(launch::async, &load_from_file, "text.txt");

    shared_future<string> shared_future_download = future_download.share();

    thread thd1(&process_text, 1, shared_future_download);
    thread thd2(&process_text, 2, shared_future_download);

    thd1.join();
    thd2.join();
}

#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/algorithm.hpp>


using namespace std;

template <typename T>
void print(const T& item)
{
    cout << item << endl;
}

template <typename T, typename... Tail>
void print(const T& head, const Tail&... tail)
{
    cout << head << " ";
    print(tail...);
}

//------------------------------------------------------------------
template <typename... Types>
struct Count;

template <typename Head, typename... Tail>
struct Count<Head, Tail...>
{
    static const int value = 1 + Count<Tail...>::value;
};

template <>
struct Count<>
{
    static const int value = 0;
};

template <typename... Types>
struct VerifyCount
{
    static const int value = sizeof...(Types);
};

// -----------------------------------

template <typename T>
constexpr T sum(const T& item)
{
    return item;
}

template <typename T, typename... Types>
constexpr T sum(const T& item, const Types&... head)
{
    return item + sum(head...);
}

template <typename... Types>
constexpr auto avg(const Types&... items)
{
    return static_cast<double>(sum(items...)) / sizeof...(items);
}

// ------------------------------
template <size_t... Ns>
struct Indexes
{};

template <typename... Ts, size_t... Ns>
auto cherry_pick(const tuple<Ts...>& t, Indexes<Ns...>)
{
    return make_tuple(get<Ns>(t)...);
}


int main()
{
    print(1, 45, 4.5, "text");

    static_assert(Count<int, double, int&, string>::value == VerifyCount<int, double, int&, string>::value,
                  "Error");

    static_assert(sum(1, 3, 4, 5, 7, 7) == 27, "Error");

    auto sum_result = sum(1, 3, 4, 5, 7, 7);
    cout << "sum_result = " << sum_result << endl;

    auto avg_result = avg(1, 54, 234, 55, 6, 23);
    cout << "avg_result = " << avg_result << endl;


    auto t = make_tuple(1, 3.14, "text"s, 2);
    auto picked = cherry_pick(t, Indexes<0, 3>{});

    cout << "picked items: \n";
    boost::fusion::for_each(picked, [](const auto& item) { cout << "item: " << item << endl;});
}

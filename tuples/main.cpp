#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>

using namespace std;

tuple<int, int, double> calc_stats(const vector<int>& data)
{
    vector<int>::const_iterator min_it, max_it;
    tie(min_it, max_it) = minmax_element(data.begin(), data.end());
    auto avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return make_tuple(*min_it, *max_it, avg);
}

int main()
{
    vector<int> data = { 1, 2, 453, 234, -6, 356 };

    tuple<int, int, double> stats = calc_stats(data);

    cout << "min: " << get<0>(stats) << endl;
    cout << "max: " << get<1>(stats) << endl;
    cout << "avg: " << get<2>(stats) << endl;

    cout << "\n\n";

    int min, max;
    //double avg;

    //tuple<int&, int&, double&> ref_stats{min, max, avg};
    //ref_stats = calc_stats(data);

    tie(min, max, ignore) = calc_stats(data);

    cout << "min: " << min << endl;
    cout << "max: " << max << endl;
    //cout << "avg: " << avg << endl;

    tuple<int, double> t1(1, 3.14);
    tuple<int, double> t2 = t1;

    cout << (t1 == t2) << endl;
}

#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <list>
#include <functional>
#include <cassert>
#include <algorithm>
#include <tuple>

using namespace std;

template <typename Iter>
Iter find_max(Iter begin, Iter end)
{
    if(begin == end)
        return end;

    Iter max_item = begin;

    for(Iter it = begin; it != end; ++it)
        if (*it > *max_item)
            max_item = it;

    return max_item;
}

template <typename Iter, typename Comp>
Iter find_max(Iter begin, Iter end, Comp comp_gt)
{
    if(begin == end)
        return end;

    Iter max_item = begin;

    for(Iter it = begin; it != end; ++it)
        if (comp_gt(*it, *max_item))
            max_item = it;

    return max_item;
}

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    bool operator<(const Person& p) const
    {
        return tie(id_, name_) > tie(p.id_, p.name_);
    }

    bool operator==(const Person& p) const
    {
        return tied() == p.tied();
    }
private:
    tuple<const int&, const string&> tied() const
    {
        return tie(id_, name_);
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id: " << p.id() << ", name: " << p.name() << ")";
    return out;
}

template <typename Container>
void print(const Container& container, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = container.begin(); it != container.end(); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}

int main()
{
    vector<int> data = { 1, 6, 234, 45, 2345, 23, 45 };

    print(data, "data");

    auto max_it = find_max(data.begin(), data.end());

    cout << "max in data: " << *max_it << endl;

    cout << "\n";

    list<string> words = { "one", "two", "three" };

    cout << "max in words: " << *find_max(words.begin(), words.end()) << endl;

    double numbers[] = { 1.87, 4.33, 55.33, 1.33 };

    cout << "max in numbers: " << *max_element(begin(numbers), end(numbers)) << endl;

    vector<Person> people = { Person{1, "Kowalski"}, Person{5, "Nowak"}, Person{2, "Anonim"} };

    auto max1 = find_max(people.begin(), people.end(),
                         [](const Person& p1, const Person& p2) { return p1.id() > p2.id(); });

    cout << "max1: " << *max1 << endl;
}

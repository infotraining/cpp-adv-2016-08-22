#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <exception>

using namespace std;

void may_throw()
{
    random_device rd;
    mt19937 random_engine(rd());
    uniform_int_distribution<> dist(1, 100);

    auto random_number = dist(random_engine);

    if (random_number % 13 == 0)
        throw runtime_error("Error#13");

    if (random_number % 5 == 0)
        throw logic_error("Error#5");
}

void background_work(int id, chrono::milliseconds delay, exception_ptr& eptr)
{
    try
    {
        for(int i = 0; i < 10; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;
            may_throw();
            this_thread::sleep_for(delay);
        }
    }
    catch(...)
    {
        eptr = current_exception();
    }

    cout << "THD#" << id << " is finished..." << endl;
}

int main()
{
    cout << "No of cores: " << thread::hardware_concurrency() << endl;

    exception_ptr eptr;

    thread thd1{&background_work, 1, 50ms, ref(eptr)};
    thd1.join();

    if (eptr)
    {
        try
        {
           rethrow_exception(eptr);
        }
        catch(const exception& e)
        {
            cout << "Caught an exception in main: " << e.what() << endl;
        }
    }

    // for group of threads

    const size_t size = 5;

    vector<thread> threads(size);
    vector<exception_ptr> eptrs(size);

    for(size_t i = 0; i < size; ++i)
        threads[i] = thread{&background_work, i+1, 50ms, ref(eptrs[i])};

    for(auto& thd: threads)
        thd.join();

    // handling an exceptions
    for(auto& eptr : eptrs)
    {
        if (eptr)
        {
            try
            {
               rethrow_exception(eptr);
            }
            catch(const runtime_error& e)
            {
                cout << "Caught an exception in main: " << e.what() << endl;
            }
            catch(const logic_error& e)
            {
                cout << "Caught an exception in main: " << e.what() << endl;
            }
        }
    }

}

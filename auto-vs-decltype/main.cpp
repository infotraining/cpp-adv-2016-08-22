#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

template <typename Vector>
auto multiply(const Vector& a, const Vector& b) //decltype(a * b)
{
    auto result = a * b;

    return result;
}

template <typename Map>
decltype(auto) find_by_key(const Map& m, const typename Map::key_type& key)
{
    return m.at(key);
}

int main()
{
    int x = 10;
    int& ref_x = x;
    const int& cref_x = x;

    auto y1 = x; // int
    auto y2 = ref_x; // int
    auto y3 = cref_x; // int

    auto& ry2 = ref_x; // int&
    auto& ry3 = cref_x; // const int&
    auto& ry4 = x; // int&

    decltype(cref_x) ry5 = x;

    vector<string> words = { "one", "two", "three" };

    for(auto& item : words)
        cout << item << " ";
    cout << endl;

//    for(auto it = words.begin(); it != words.end(); ++it)
//    {
//        const auto& item = *it; // string

//        cout << item << " ";
//    }
}

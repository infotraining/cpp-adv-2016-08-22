#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <cstddef>
#include <initializer_list>
#include <algorithm>
#include <iostream>
#include <memory>

template <typename T>
class Vector
{
public:
    typedef T* iterator;
    using const_iterator = const T*;

    Vector(size_t size) : size_{size}, items_{new T[size_]}
    {
        std::fill(items_.get(), items_.get() + size, T{});

        std::cout << "Vector(";
        print();
        std::cout << ")" << std::endl;
    }

    Vector(std::initializer_list<T> il);

    // copy constructor
    Vector(const Vector& source) : size_{source.size_}, items_{new T[size_]}
    {
        std::copy(source.begin(), source.end(), items_.get());

        std::cout << "Vector(cc - ";
        print();
        std::cout << ")" << std::endl;
    }

    // copy assignment
//    Vector& operator=(const Vector& source)
//    {
//        if (this != &source)
//        {
//            std::unique_ptr<T[]> temp_items(new T[source.size_]);
//            std::copy(source.begin(), source.end(), temp_items.get());

//            items_ = move(temp_items);
//            size_ = source.size_;
//        }

//        std::cout << "Vector op=(cc - ";
//        print();
//        std::cout << ")" << std::endl;

//        return *this;
//    }

    Vector& operator=(const Vector& source)
    {
        Vector temp(source);
        temp.swap(*this);

        std::cout << "Vector op=(cc - ";
        print();
        std::cout << ")" << std::endl;

        return *this;
    }

    void swap(Vector& other)
    {
        std::swap(size_, other.size_);
        items_.swap(other.items_);
    }

    // move constructor
    Vector(Vector&& source) noexcept : size_{source.size_}, items_{move(source.items_)}
    {
        source.size_ = 0;

        std::cout << "Vector(mv - ";
        print();
        std::cout << ")" << std::endl;
    }

    // copy assignment
    Vector& operator=(Vector&& source) noexcept
    {
        if (this != &source)
        {
            items_ = move(source.items_);
            size_ = source.size_;

            source.size_ = 0;
        }

        std::cout << "Vector op=(mv - ";
        print();
        std::cout << ")" << std::endl;

        return *this;
    }

    T& operator[](size_t index)
    {
        return items_[index];
    }

    const T& operator[](size_t index) const
    {
        return items_[index];
    }

    ~Vector()
    {
        std::cout << "~Vector(";
        print();
        std::cout << ")" << std::endl;
    }

    size_t size() const
    {
        return size_;
    }

    iterator begin()
    {
        return items_.get();
    }

    iterator end()
    {
        return items_.get() + size_;
    }

    const_iterator begin() const
    {
        return items_.get();
    }

    const_iterator end() const
    {
        return items_.get() + size_;
    }


private:
    size_t size_;
    std::unique_ptr<T[]> items_;

    void print()
    {
        std::cout << "[ ";
        for(size_t i = 0; i < size_; ++i)
            std::cout << items_[i] << " ";
        std::cout << "]";
    }
};

template <typename T>
Vector<T>::Vector(std::initializer_list<T> il) : size_{il.size()}, items_{new T[size_]}
{
    std::copy(il.begin(), il.end(), items_.get());

    std::cout << "Vector(";
    print();
    std::cout << ")" << std::endl;
}


#endif // VECTOR_HPP

#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <type_traits>

#include "vector.hpp"

using namespace std;

string foo(string str)
{
    return "foo(" + str + ")";
}

void reference_binding()
{
    string text;

    string& ref_text = text; // ok in C++98 && C++11
    const string& ref_text2 = foo("text"); // ok in C++98 && C++11
    //string& ref_text2 = foo("text"); // error in C++98 && C++11

    string&& rref_text = foo("text");
    //string&& rref_text2 = text; // error in C++11
}

template <typename T>
void my_swap(T& a, T& b)
{
    T temp = move(a);
    a = move(b);
    b = move(temp);
}

Vector<int> load_data(int seed)
{
    Vector<int> data(25);
    iota(data.begin(), data.end(), seed);

    return data;
}

class Data
{
    Vector<int> data_;
public:
    Data(size_t size, int seed) : data_(size)
    {
        iota(data_.begin(), data_.end(), seed);
        cout << "Data(" << size << ")" << endl;
    }
};


namespace MoveWithDestructor
{
    class Data
    {
        Vector<int> data_;
    public:
        Data(size_t size, int seed) : data_(size)
        {
            iota(data_.begin(), data_.end(), seed);
            cout << "Data(" << size << ")" << endl;
        }

        Data(const Data&) = default;
        Data& operator=(const Data&) = default;

        Data(Data&&) = default;
        Data& operator=(Data&&) = default;

        ~Data()
        {
            cout << "~Data()" << endl;
        }
    };
}

int main()
{
    //reference_binding(); // ok in C++11

    Vector<int> vec1 = { 1, 2, 3, 4, 5 };
    Vector<int> vec2 = { 6, 7, 8 };

    my_swap(vec1, vec2);

    cout << "\n\n";

    using Container = vector<Vector<int>>;

    Container container;

    container.push_back(Vector<int>{2, 3, 4, 5, 6, 7});
    container.push_back(load_data(1));

    Vector<int> data1 = load_data(100);

    container.push_back(move(data1));

    cout << "\n\n";

    Data dataA(10, 200);
    Data dataB(5, 500);

    my_swap(dataA, dataB);

    static_assert(is_nothrow_move_constructible<Data>::value, "Data&& throws");
    static_assert(is_nothrow_move_assignable<Data>::value, "Data&& throws");

    cout << "\nEnd of main..." << endl;
}

#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    using mutex_type = std::recursive_mutex;

    const int id_;
    double balance_;
    mutable mutex_type mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = ";

        double temp_balance;

        {
            std::lock_guard<mutex_type> lk{mtx_};
            temp_balance = balance_;
        }

        std::cout << temp_balance << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<mutex_type> lk_from{mtx_, std::defer_lock};
        std::unique_lock<mutex_type> lk_to{to.mtx_, std::defer_lock};

        std::lock(lk_from, lk_to);

        withdraw(amount);
        to.deposit(amount);
    }

    void withdraw(double amount)
    {
        std::lock_guard<mutex_type> lk{mtx_};
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<mutex_type> lk{mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<mutex_type> lk{mtx_};
        return balance_;
    }
};

void make_withdraw(BankAccount& ba, size_t no_of_operations)
{
    for(size_t i = 0; i < no_of_operations; ++i)
        ba.withdraw(1.0);
}

void make_deposit(BankAccount& ba, size_t no_of_operations)
{
    for(size_t i = 0; i < no_of_operations; ++i)
        ba.deposit(1.0);
}

void make_transfer(BankAccount& from, BankAccount& to, size_t no_of_operations, double amount)
{
    for(size_t i = 0; i < no_of_operations; ++i)
    {
        //std::cout << "transfer from " << from.id() << " to " << to.id() << std::endl;
        from.transfer(to, amount);
    }
}

int main()
{
    using namespace std;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    cout << "\n\n";

    const int N = 10'000'000;

    thread thd1{&make_withdraw, ref(ba1), N};
    thread thd2{&make_deposit, ref(ba1), N};

    thd1.join();
    thd2.join();

    ba1.print();
    ba2.print();

    cout << "\n\n";

    thread thd3{&make_transfer, ref(ba1), ref(ba2), N/10, 1.0};
    thread thd4{&make_transfer, ref(ba2), ref(ba1), N/10, 1.0};
    thd3.join();
    thd4.join();

    ba1.print();
    ba2.print();
}

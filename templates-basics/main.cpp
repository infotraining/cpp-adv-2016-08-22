#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <type_traits>
#include <cstring>

using namespace std;

template <typename T>
const T& maximum(const T& a, const T& b)
{
    return (a < b) ? b : a;
}

template <typename T>
const T& maximum(T* a, T* b)
{
    return (*a < *b) ? *b : *a;
}

const char* maximum(const char* txt1, const char* txt2)
{
    return strcmp(txt1, txt2) > 0 ? txt1 : txt2;
}

//double maximum(double, double) = delete;

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_{id}, name_{name}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    bool operator<(const Person& p) const
    {
        if (id() == p.id())
            return name() < p.name();

        return id() < p.id();
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id: " << p.id() << ", name: " << p.name() << ")";
    return out;
}

int main()
{
    int a = 10;
    int b = 24;

    cout << "maximum(10, 24): " << maximum(a, b) << endl;

    cout << "maximum(5.22, 4.55): " << maximum(5.22, 4.55) << endl;

    cout << "maximum(75.55, 24): " << maximum(75.55, static_cast<double>(b)) << endl;

    cout << "maximum(7.55, 24): " << maximum<int>(7.55, b) << endl;

    string str1 = "ala";
    string str2 = "ola";

    cout << "maximum(str1, str2): " << maximum(str1, str2) << endl;

    Person p1{ 9, "Kowalski" };
    Person p2{ 5, "Nowak" };

    cout << "maximum(p1, p2): " << maximum(p1, p2) << endl;

    const char* txt2 = "iwa";
    const char* txt1 = "ewa";

    cout << "maximum(txt1, txt2): " << maximum(txt1, txt2) << endl;

    cout << "maximum(&p1, &p2): " << maximum(&p1, &p2) << endl;
}

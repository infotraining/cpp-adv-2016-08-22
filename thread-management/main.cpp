#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>

using namespace std;

void hello()
{
    std::string text = "Hello Concurrent World!";

    for(const auto& c : text)
    {
        cout << c << " "s;
        this_thread::sleep_for(50ms);
    }

    cout << endl;
}

void background_work(int id, chrono::milliseconds delay)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;
        this_thread::sleep_for(delay);
    }

    cout << "THD#" << id << " is finished..." << endl;
}

class BackgroundWork
{
    const int id_;

public:
    BackgroundWork(int id) : id_{id}
    {}

    void operator()(chrono::milliseconds delay)
    {
        for(int i = 0; i < 10; ++i)
        {
            cout << "BW#" << id_ << ": " << i << endl;
            this_thread::sleep_for(delay);
        }

        cout << "BW#" << id_ << " is finished..." << endl;
    }
};

class JoinThreadGuard
{
    thread& thd_;
public:
    JoinThreadGuard(thread& thd) : thd_{thd}
    {}

    JoinThreadGuard(const JoinThreadGuard&) = delete;
    JoinThreadGuard& operator=(const JoinThreadGuard&) = delete;

    ~JoinThreadGuard()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

void may_throw()
{
    random_device rd;
    mt19937 random_engine(rd());
    uniform_int_distribution<> dist(1, 100);

    auto random_number = dist(random_engine);

    if (random_number % 2)
        throw runtime_error("Error");
}

int main()
{
    thread thd1(&hello);
    thread thd2(&hello);

    thread thd3{&background_work, 1, 200ms};
    thread thd4{&background_work, 2, 500ms};

    BackgroundWork bw(1);
    thread thd5{bw, 200ms};
    thread thd6{BackgroundWork(2), 700ms};

    thd1.join();
    thd2.join();

    thd3.join();
    thd4.detach();

    thd5.join();
    thd6.join();

    const vector<int> source_data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    vector<int> target1(source_data.size());
    vector<int> target2(source_data.size());

    try
    {
        thread thd7{[&source_data, &target1]() { copy(source_data.begin(), source_data.end(), target1.begin()); }};
        JoinThreadGuard thd7_guard{thd7};

        thread thd8{[&source_data, &target2]() { copy(source_data.begin(), source_data.end(), target2.begin()); }};
        JoinThreadGuard thd8_guard{thd8};

        may_throw();
    }
    catch(const runtime_error& e)
    {
        cout << "Caught an exception: " << e.what() << endl;
    }

    for(const auto& item : target1)
        cout << item << " ";
    cout << endl;

    for(const auto& item : target2)
         cout << item << " ";
    cout << endl;
}

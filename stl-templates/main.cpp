#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <array>
#include <deque>
#include <list>
#include <map>

using namespace std;

template <typename T>
ostream& operator<<(ostream& out, const unique_ptr<T>& ptr)
{
    out << "uptr(" << *ptr << ")";
    return out;
}

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "{ "<< p.first << " , " << p.second << " }";
    return out;
}

template <typename Container>
void print(const Container& container, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = container.begin(); it != container.end(); ++it)
    {
        cout << *it << " ";
    }
    cout << "]" << endl;
}

template <typename T>
struct Greater
{
    bool operator()(const T& a, const T& b) const
    {
        return a > b;
    }
};

class Magic_23523423
{
public:
    template <typename T1, typename T2>
    bool operator()(T1 a, T2 b) { return a > b; }
};

template <typename Vector>
auto foo(const Vector& a, const Vector& b) -> decltype(a * b)
{
    auto result = a * b;

    return result;
}

int main()
{
    // array
    array<int, 10> arr1 = {1, 2, 3, 4};

    print(arr1, "arr1");

    // vector
    vector<string> vec = { "1", "2", "3", "4", "5", "6" };
    print(vec, "vec");

    vector<string>::iterator it_vec1 = vec.begin() + 2;
    vector<string>::iterator it_vec2 = vec.end() - 2;
    string* ptr_item = &vec[3];
    string& ref_item = vec[4];

    //vec.push_back("9"); // it invalidates iterators, ptrs & refs

    cout << *it_vec1 << "  " << *it_vec2 << endl;
    cout << "size of range: " << (it_vec2 - it_vec1) << endl;

    // deque
    deque<int> dq1 = { 1, 2, 3, 4, 5 };

    print(dq1, "dq1");

    auto it_dq1 = dq1.begin() + 2;
    int* ptr_number = &dq1[3];
    int& ref_number = dq1[0];

    dq1.push_back(-6); // invalidates iterators
    dq1.push_front(-9); // invalidates iterators

    cout << *ptr_number << " " << ref_number << endl;

    dq1[4] = -100;

    print(dq1, "dq1");

    // list
    list<int> lst1 = { 1, 2, 3, 3, 3, 3, 4, 5, -6, 3, 12, 2, 3, 54 };

    list<int>::iterator it_lst = lst1.begin();
    int* ptr_number2 = &(*it_lst);
    int& ref_number2 = *it_lst;

    lst1.push_front(-9); // nothing is invalidated
    lst1.push_back(35); // nothing is invalidated

    print(lst1, "lst1 before");

    lst1.sort(greater<int>());
    lst1.sort([](auto a, auto b) { return a > b; });
    //lst1.sort(Magic_23523423());

    lst1.unique();

    print(lst1, "lst1 after");

    // map

    map<int, string, greater<int>> dict = { {1, "one"}, {3, "three"}, {2, "two"} };

    pair<map<int, string, greater<int>>::iterator, bool> result = dict.insert(make_pair(5, "five"));

    dict[4] = "four";

    print(dict, "dict");

    using IntPtr = unique_ptr<int>;

    auto compare_by_val = [](const auto& a, const auto& b) -> bool
    {
        if ()
           return *a < *b;
        else
           return
    };

    map<IntPtr, string, decltype(compare_by_val)> dict_with_ptr(compare_by_val);

    dict_with_ptr.insert(make_pair(make_unique<int>(2), "two"));
    dict_with_ptr.insert(make_pair(make_unique<int>(1), "one"));
    dict_with_ptr.insert(make_pair(make_unique<int>(4), "four"));
    dict_with_ptr.insert(make_pair(make_unique<int>(3), "three"));

    print(dict_with_ptr, "dict_with_ptr");
}

#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <array>
#include <list>

using namespace std;

template <typename T>
class Vector
{
public:
    typedef T* iterator;
    using const_iterator = const T*;

    Vector(size_t size) : size_{size}, items_{new T[size_]}
    {
        fill(items_, items_ + size, T{});
    }

    Vector(initializer_list<T> il);

    Vector(const Vector& source) : size_{source.size_}, items_{new T[size_]}
    {
        copy(source.begin(), source.end(), items_);
    }

    Vector& operator=(const Vector& source)
    {
        if (this != &source)
        {
            //(***)
            delete [] items_;
            items_ = new T[source.size_];

            copy(source.begin(), source.end(), items_);
        }

        return *this;
    }

    T& operator[](size_t index)
    {
        return items_[index];
    }

    const T& operator[](size_t index) const
    {
        return items_[index];
    }

    ~Vector()
    {
        delete [] items_;
    }

    size_t size() const
    {
        return size_;
    }

    iterator begin()
    {
        return items_;
    }

    iterator end()
    {
        return items_ + size_;
    }

    const_iterator begin() const
    {
        return items_;
    }

    const_iterator end() const
    {
        return items_ + size_;
    }


private:
    size_t size_;
    T* items_;
};

template <typename T>
Vector<T>::Vector(initializer_list<T> il) : size_{il.size()}, items_{new T[size_]}
{
    copy(il.begin(), il.end(), items_);
}


//-----------------
namespace TemplateParams1
{
    template <typename T, size_t N>
    class Stack
    {
    public:
        Stack() : size_{0}
        {}

        void push(const T& item)
        {
            items_[size_++] = item;
        }

        T pop()
        {
            T& item = items_[size_-1];
            --size_;
            return item;
        }

        bool empty() const
        {
            return size_ == 0;
        }

    private:
        size_t size_;
        T items_[N];
    };
}

namespace TemplateParams2
{
    template <typename T, typename Container = vector<T>>
    class Stack
    {
    public:
        Stack()
        {}

        void push(const T& item)
        {
            items_.push_back(item);
        }

        T pop()
        {
            T item = items_.back();

            items_.pop_back();
            return item;
        }

        bool empty() const
        {
            return items_.empty();
        }

    private:
        Container items_;
    };
}

namespace TemplateParams3
{
    template
    <
        typename T,
        template<typename, typename> class Container,
        typename Alloc = std::allocator<T>
    >
    class Stack
    {
    public:
        using iterator = typename Container<T, Alloc>::iterator;
        using const_iterator = typename Container<T, Alloc>::const_iterator;

        Stack()
        {}

        template <typename T2, template<typename, typename> class Container2, typename Alloc2>
        Stack(const Stack<T2, Container2, Alloc2>& source)
        {
            for(const auto& item : source)
            {
                items_.push_back(item);
            }
        }

        void push(const T& item)
        {
            items_.push_back(item);
        }

        T pop()
        {
            T item = items_.back();

            items_.pop_back();
            return item;
        }

        bool empty() const
        {
            return items_.empty();
        }

        size_t size() const
        {
            return items_.size();
        }

        iterator begin()
        {
            return items_.begin();
        }

        iterator end()
        {
            return items_.end();
        }

        const_iterator begin() const
        {
            return items_.begin();
        }

        const_iterator end() const
        {
            return items_.end();
        }

    private:
        Container<T, Alloc> items_;
    };
}


int main()
{
    Vector<int> vec1(10);

    vec1[3] = 423;
    vec1[9] = 88;

    for(size_t i = 0; i < vec1.size(); ++i)
        cout << vec1[i] << " ";
    cout << endl;


    Vector<int> vec2 = { 1, 2, 3, 4, 5, 6, 7 };

    for(const auto& item : vec2)
        cout << item << " ";
    cout << endl;

    Vector<int> vec3 = vec2;



    TemplateParams1::Stack<int, 10> stack;

    for(int i = 0; i < 5; ++i)
    {
        stack.push(i);
    }

    cout << "\nStack1: ";
    while(!stack.empty())
    {
        cout << stack.pop() << " " ;
    }

    cout << endl;

    TemplateParams2::Stack<int, list<int>> stack2;

    for(int i = 0; i < 5; ++i)
    {
        stack2.push(i);
    }

    cout << "\nStack2: ";
    while(!stack2.empty())
    {
        cout << stack2.pop() << " " ;
    }

    cout << endl;

    TemplateParams3::Stack<int, list> stack3;

    for(int i = 0; i < 5; ++i)
    {
        stack3.push(i);
    }

    cout << "\nStack3: ";
    while(!stack3.empty())
    {
        cout << stack3.pop() << " " ;
    }

    cout << endl;

    for(int i = 0; i < 5; ++i)
    {
        stack3.push(i);
    }

    TemplateParams3::Stack<double, list> stack4 = stack3;

    cout << "\nStack4: ";
    while(!stack4.empty())
    {
        cout << stack4.pop() << " " ;
    }

    cout << endl;

}

#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>
#include <future>

using namespace std;

void calc_hits_with_ref(const unsigned long no_of_throws, unsigned long& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    unsigned long local_hits = 0;

    for (unsigned long n = 0 ; n < no_of_throws ; ++n)
    {

        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            ++local_hits;
    }

    hits += local_hits;
}

unsigned long calc_hits(const unsigned long no_of_throws)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    unsigned long hits = 0;

    for (unsigned long n = 0 ; n < no_of_throws ; ++n)
    {

        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            ++hits;
    }

    return hits;
}

int main()
{
    unsigned long N = 10'000'000;
    unsigned long counter = 0;

    cout << "Pi calc in 1 thread!" << endl;
    auto start = chrono::high_resolution_clock::now();

    calc_hits_with_ref(N, counter);

    double pi = double(counter)/double(N)*4;

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi = " << pi << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    // -----------------------------
    const size_t no_of_hardware_threads = max(1U, thread::hardware_concurrency());

    cout << "Pi calc in " << no_of_hardware_threads<< " threads!" << endl;

    start = chrono::high_resolution_clock::now();

    vector<thread> threads(no_of_hardware_threads);
    vector<unsigned long> partial_hits(no_of_hardware_threads);

    for(size_t i = 0; i < no_of_hardware_threads; ++i)
    {
        threads[i] = thread{&calc_hits_with_ref, N/no_of_hardware_threads, ref(partial_hits[i])};
    }

    for(auto& thd : threads)
        thd.join();

    unsigned long total_hits = accumulate(partial_hits.begin(), partial_hits.end(), 0ul);

    pi = double(total_hits)/double(N)*4;

    end = chrono::high_resolution_clock::now();

    cout << "Pi = " << pi << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    // -----------------------------
    cout << "Pi calc in " << no_of_hardware_threads<< " async functions!" << endl;

    start = chrono::high_resolution_clock::now();

    vector<future<unsigned long>> future_hits(no_of_hardware_threads);


    for(size_t i = 0; i < no_of_hardware_threads; ++i)
    {
        future_hits[i] = async(launch::async, &calc_hits, N/no_of_hardware_threads);
    }

    total_hits = accumulate(future_hits.begin(), future_hits.end(), 0UL,
                            [](auto& total, auto& f) { return total + f.get(); });

    pi = double(total_hits)/double(N)*4;

    end = chrono::high_resolution_clock::now();

    cout << "Pi = " << pi << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;


    return 0;
}

